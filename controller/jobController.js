var Job= require('../models/job');

exports.createJob=function (req,res) {

    var jobObj = new Job(req.body);
    jobObj.jobOwner=req.loggedInMember._id;
    jobObj.save(function (err) {
        if (err) {
            return res.status(500).send(err);
        } else {
            var page = 1;
            var limit = 10;
            Job.find().paginate(parseInt(page), parseInt(limit),
                function(err, listData, totalItem) {
            var responseData = {
                'title': 'Express',
                'username': req.loggedInMember.username,
                'fullName': req.loggedInMember.fullName,
                'listData': listData,
                'memberId':req.loggedInMember._id,
                'totalPage': Math.ceil(totalItem/limit),
                'page': page,
                'limit': limit
            };

            res.render('member/home', responseData);
        });
    }

})
}


// require('mongoose-pagination');

exports.getList = function (req, resp) {
    var page = req.query.page;
    var limit = req.query.limit;
    var responseData;
    Job.find().paginate(parseInt(page), parseInt(limit),
        function(err, listData, totalItem) {
            responseData = {
                'listData': listData,
                'totalPage': Math.ceil(totalItem/limit),
                'page': page,
                'limit': limit
            };
            resp.render('member/home', responseData);
        });
}

exports.findJob=function (req,res) {
    var categories = req.body.jobCategories;
    var location = req.body.jobLocation;
    var totalTime = req.body.totalTime;
    var page = 1;
    var limit = 5;

    var xoo;

    var query;
    query = Job.find();
    if (categories !== 'none') {
        query = query.where('jobCategories').equals(categories)
        console.log('this is query jobcate gories'+query)
    }
    if (location !== 'none') {
        query = query.where('jobLocation').equals(location)
    }
    if (totalTime !== 'none') {
        console.log('Test loi'+parseInt(totalTime))
        query = query.where('totalTime').equals(parseInt(totalTime))
        query.find().paginate(parseInt(page), parseInt(limit), function (err,jobResult,totalItem) {
            xoo = {
                'jobRes': jobResult,
                'title': 'Express',
                'username': req.loggedInMember.username,
                'fullName': req.loggedInMember.fullName,
                'memberId':req.loggedInMember._id,
                'totalPage': Math.ceil(totalItem/limit),
                'page': page,
                'limit': limit
            }
            res.render('Job/list', xoo)
        })
    } else {
        query.find().paginate(parseInt(page), parseInt(limit), function (err,jobResult, totalItem) {
            xoo = {
                'jobRes': jobResult,
                'title': 'Express',
                'username': req.loggedInMember.username,
                'fullName': req.loggedInMember.fullName,
                'memberId':req.loggedInMember._id,
                'totalPage': Math.ceil(totalItem/limit),
                'page': page,
                'limit': limit
            }
            res.render('Job/list', xoo)
        })
    }


}
//

// exports.findJob=function (req,res) {
//     return res.redirect('/');
// }