var Job= require('../models/job');

exports.createJob=function (req,res) {

    var jobObj = new Job(req.body);
    jobObj.jobOwner=req.loggedInMember._id;
    jobObj.save(function (err) {
        if (err) {
            return res.status(500).send(err);
        } else {
            var page = 1;
            var limit = 10;
            Job.find().paginate(parseInt(page), parseInt(limit),
                function(err, listData, totalItem) {
            var responseData = {
                'title': 'Express',
                'username': req.loggedInMember.username,
                'fullName': req.loggedInMember.fullName,
                'listData': listData,
                'memberId':req.loggedInMember._id,
                'totalPage': Math.ceil(totalItem/limit),
                'page': page,
                'limit': limit
            };

            res.render('member/home', responseData);
        });
    }

})
}


// require('mongoose-pagination');

exports.getList = function (req, resp) {
    var page = req.query.page;
    var limit = req.query.limit;
    var responseData;
    Job.find().paginate(parseInt(page), parseInt(limit),
        function(err, listData, totalItem) {
            responseData = {
                'listData': listData,
                'totalPage': Math.ceil(totalItem/limit),
                'page': page,
                'limit': limit
            };
            resp.render('member/home', responseData);
        });
}

exports.findJob=function (req,res) {
    var categories = req.body.jobCategories;
    var location = req.body.jobLocation;
    var totalTime = req.body.totalTime;
    var page = 1;
    var limit = 5;

    var xoo;

    var query;
    query = Job.find();
    if (categories !== 'none') {
        query = query.where('jobCategories').equals(categories)
        console.log('this is query jobcate gories'+query)
    }
    if (location !== 'none') {
        query = query.where('jobLocation').equals(location)
    }
    if (totalTime !== 'none') {
        console.log('Test loi'+parseInt(totalTime))
        query = query.where('totalTime').equals(parseInt(totalTime))
        query.find().paginate(parseInt(page), parseInt(limit), function (err,jobResult,totalItem) {
            xoo = {
                'jobRes': jobResult,
                'title': 'Express',
                'username': req.loggedInMember.username,
                'fullName': req.loggedInMember.fullName,
                'memberId':req.loggedInMember._id,
                'totalPage': Math.ceil(totalItem/limit),
                'page': page,
                'limit': limit
            }
            res.render('Job/list', xoo)
        })
    } else {
        query.find().paginate(parseInt(page), parseInt(limit), function (err,jobResult, totalItem) {
            xoo = {
                'jobRes': jobResult,
                'title': 'Express',
                'username': req.loggedInMember.username,
                'fullName': req.loggedInMember.fullName,
                'memberId':req.loggedInMember._id,
                'totalPage': Math.ceil(totalItem/limit),
                'page': page,
                'limit': limit
            }
            res.render('Job/list', xoo)
        })
    }


}

// exports.assignJob=function (req,res) {
//     var jobId= req.query.jobId;
//     console.log('JOB ID: '+jobId)
//     var assignId=req.loggedInMember._id;
//     console.log('ASSIGN ID: '+assignId)
//
//
// //Hàm đổi jobstatus =0 và thêm Id người Assign
//     Job.findByIdAndUpdate({_id:jobId},{jobStatus:0,jobAssign: assignId},{new:true},function (err,todo) {
//             if(err){return res.status(500).send(err)};
//             console.log('done')
//
//         }
//     )
//     //Hàm đổi JobId vào danh sách công việc của người assign
//     Member.findByIdAndUpdate({_id:assignId},{$push:{jobToDo: jobId}},{new:true},function (err,todo) {
//             if(err){return res.status(500).send(err)};
//             res.redirect('/members/login')
//
//         }
//
//     )
//
//
// }
//

// exports.findJob=function (req,res) {
//     return res.redirect('/');
// }